;;; Cybo - Cyber Organizer
;;; Copyright © 2017, 2018, 2019 Jeremy Korwin
;;;
;;; This program is free software: you can redistribute it and/or modify it under
;;; the terms of the GNU General Public License as published by the Free Software
;;; Foundation; either version 3 of the License, or (at your option) any later
;;; version.
;;;
;;; This program is distributed in the hope that it will be useful, but WITHOUT
;;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License along with
;;; this program. If not, see http://www.gnu.org/licenses/.

;; -*- geiser-scheme-implementation: guile -*-

(define-module (cybo)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 regex)
  #:use-module (srfi srfi-1))

(define UTF-8_ENCODING "UTF-8")
(define EMPTY_LIST '())
(define COMPLETION_MARK "x ")
(define COMPLETION_MARK_INDEX 0)
(define COMPLETION_MARK_LENGTH 2)
(define PRIORITY_MARK_INDEX 0)
(define PRIORITY_MARK_LENGTH 3)
(define PRIORITY_MARK_REGEX "^\\([A-Z]\\) ")
(define CONTEXT_MARK " @")
(define TOP_OF_THE_LIST_INDEX 1)

(define-public done
  (lambda (filename)
    (let ((all-tasks (read-tasks-from filename)) (current-task (next-action filename)))
      (if (not (null? current-task))
	  (call-with-output-file filename
	    (lambda (file-port)
	      (for-each (lambda (task)
			  (if (string=? (car current-task) task)
			      (display (string-append COMPLETION_MARK (car current-task) "\n") file-port)
			      (display (string-append task "\n") file-port)))
			all-tasks))
	    #:encoding UTF-8_ENCODING)))))

(define-public next-action
  (lambda* (filename #:key context)
    (oldest
     (filter-for-highest-priority
      (filter-for-context context
			  (filter-for-remaining-tasks
			   (read-tasks-from filename)))))))

(define (filter-for-context context task-list)
  (if context
      (filter (lambda (task)
		(string-contains task (string-append CONTEXT_MARK context)))
	      task-list)
      task-list))

(define (filter-for-highest-priority task-list)
  (let ((prioritized (filter-for-prioritized task-list)))
    (if (null? prioritized) task-list (sort-by-priority prioritized))))

(define (filter-for-prioritized task-list)
  (filter match-prioritized task-list))

(define (match-prioritized task)
  (string-match PRIORITY_MARK_REGEX task))

(define (sort-by-priority task-list)
  (sort-list task-list priority<?))

(define (priority<? task1 task2)
  (string<? (priority task1) (priority task2)))

(define (priority task)
  (substring task PRIORITY_MARK_INDEX PRIORITY_MARK_LENGTH))

(define (filter-for-remaining-tasks task-list)
  (filter remaining? task-list))

(define (remaining? task)
  (not (equal? COMPLETION_MARK (completion task))))

(define (completion task)
  (substring task COMPLETION_MARK_INDEX COMPLETION_MARK_LENGTH))

(define (oldest task-list)
  (if (null? task-list)
      EMPTY_LIST
      (list-head task-list TOP_OF_THE_LIST_INDEX)))

(define (read-tasks-from filename)
  (if (file-exists? filename) (lines->list filename) EMPTY_LIST))

(define (lines->list filename)
  (call-with-input-file filename
    (lambda (file-port)
      (let loop ((line (read-line file-port)) (task-list '()))
        (if (eof-object? line)
            task-list
            (loop (read-line file-port) (append-well-formatted-task task-list line)))))
    #:encoding UTF-8_ENCODING))

(define (append-well-formatted-task task-list task)
  (let ((trimmed-task (string-trim-both task #\space)))
      (if (string-null? trimmed-task)
       task-list
       (append task-list (list trimmed-task)))))

