;;; Cybo - Cyber Organizer
;;; Copyright © 2017, 2018, 2019 Jeremy Korwin
;;;
;;; This program is free software: you can redistribute it and/or modify it under
;;; the terms of the GNU General Public License as published by the Free Software
;;; Foundation; either version 3 of the License, or (at your option) any later
;;; version.
;;;
;;; This program is distributed in the hope that it will be useful, but WITHOUT
;;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License along with
;;; this program. If not, see http://www.gnu.org/licenses/.

;; -*- geiser-scheme-implementation: guile -*-

(use-modules
 (srfi srfi-64)
 (ice-9 rdelim)
 (cybo))

(module-define! (resolve-module '(srfi srfi-64)) 'test-log-to-file #t)

;;; Tools

(define (clear-test-files)
  (let ((files (list TODO.TXT)))
    (map (lambda (filename) (if (access? filename F_OK) (delete-file filename)))
         files)))

(define (file-empty filename)
  (let ((todo.txt-port (open-output-file filename #:encoding "UTF-8")))
    (close todo.txt-port)))

(define (append-tasks-to-file filename task-list)
  (let ((todo.txt-port (open-file filename "a" #:encoding "UTF-8")))
    (map (lambda (task) (write-line task todo.txt-port)) task-list)
    (close todo.txt-port)))

;;; Tests

;; next-action

(define TODO.TXT "todo.txt")

(test-begin "cybo-harness")

(test-assert "Next-action-without-todo.txt-file-Should-return-null"
  (null? (next-action TODO.TXT)))

(file-empty TODO.TXT)
(test-assert "Next-action-with-empty-todo.txt-file-Should-return-null"
  (null? (next-action TODO.TXT)))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("task to do"))
(test-equal "Next-action-with-one-task-to-do-Should-return-this-task"
  '("task to do")
  (next-action TODO.TXT))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("another task to do"))
(test-equal "Next-action-with-one-task-to-do-Should-return-this-task-Bis"
  '("another task to do")
  (next-action TODO.TXT))
(clear-test-files)

(append-tasks-to-file TODO.TXT '(""))
(test-assert "Next-action-with-one-blank-task-Should-return-null"
  (null? (next-action TODO.TXT)))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("                 "))
(test-assert "Next-action-with-only-spaces-task-Should-return-null"
  (null? (next-action TODO.TXT)))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("   task to trim  "))
(test-equal "Next-action-with-one-task-with-spaces-at-start-and-end-Should-return-the-task-trimmed"
  '("task to trim")
  (next-action TODO.TXT))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("first task to do" "second task to do"))
(test-equal "Next-action-with-two-tasks-to-do-Should-return-the-first-task"
  '("first task to do")
  (next-action TODO.TXT))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("x one completed task"))
(test-assert "Next-action-with-one-completed-task-Should-return-null"
  (null? (next-action TODO.TXT)))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("x the completed task" "the remaining task"))
(test-equal "Next-action-with-one-completed-task-and-one-remaining-task-Should-return-the-remaining-task"
  '("the remaining task")
  (next-action TODO.TXT))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("dummy task" "(A) dummy prioritized task"))
(test-equal "Next-action-with-one-prioritized-task-and-one-not-prioritized-task-Should-return-the-prioritized-task"
  '("(A) dummy prioritized task")
  (next-action TODO.TXT))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("another dummy task" "(B) another dummy prioritized task"))
(test-equal "Next-action-with-one-prioritized-task-and-one-not-prioritized-task-Should-return-the-prioritized-task-Bis"
  '("(B) another dummy prioritized task")
  (next-action TODO.TXT))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("(D) dummy prioritized task" "(A) another dummy prioritized task"))
(test-equal "Next-action-with-two-prioritized-tasks-of-different-priorities-Should-return-the-highest-priority-task"
  '("(A) another dummy prioritized task")
  (next-action TODO.TXT))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("(B) dummy prioritized task" "(B) another dummy identically prioritized task"))
(test-equal "Next-action-with-two-identically-prioritized-tasks-Should-return-the-first-task"
  '("(B) dummy prioritized task")
  (next-action TODO.TXT))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("x task0" "task1" "(C) task2" "x (A) task3" "(C) task4"))
(test-equal "Next-action-with-tasks-completed-prioritized-or-not-Should-return-the-oldest-task-with-highest-priority"
  '("(C) task2")
  (next-action TODO.TXT))
(clear-test-files)

;; done

(done TODO.TXT)
(test-assert "Next-action-after-Done-without-todo.txt-file-Should-return-null"
  (null? (next-action TODO.TXT)))

(file-empty TODO.TXT)
(done TODO.TXT)
(test-assert "Next-action-after-Done-with-empty-todo.txt-file-Should-return-null"
  (null? (next-action TODO.TXT)))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("one task to do"))
(done TODO.TXT)
(test-assert "Next-action-after-Done-with-one-task-to-do-Should-return-null"
  (null? (next-action TODO.TXT)))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("first task to do" "second task to do"))
(done TODO.TXT)
(test-equal "Next-action-after-Done-with-two-task-to-do-Should-return-the-second-task"
  '("second task to do")
  (next-action TODO.TXT))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("x task done"))
(done TODO.TXT)
(test-assert "Next-action-after-Done-with-one-completed-task-Should-return-null"
  (null? (next-action TODO.TXT)))
(clear-test-files)

;; context

(file-empty TODO.TXT)
(test-assert "Next-action-with-context-parameter-with-empty-todo.txt-file-Should-return-null"
  (null? (next-action TODO.TXT #:context "the-context")))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("task without context"))
(test-assert "Next-action-with-context-parameter-with-one-task-not-contextualized-Should-return-null"
  (null? (next-action TODO.TXT #:context "the-context")))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("task with @another-context"))
(test-assert "Next-action-with-context-parameter-with-one-task-with-another-context-Should-return-null"
  (null? (next-action TODO.TXT #:context "the-context")))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("task with @the-context"))
(test-equal "Next-action-with-context-parameter-with-one-task-with-the-context-Should-return-the-task"
  '("task with @the-context")
  (next-action TODO.TXT #:context "the-context"))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("task with @another-context"
				 "task with @the-context"
				 "(A) task without context"))
(test-equal "Next-action-with-context-parameter-with-tasks-only-one-of-which-with-the-context-Should-return-this-task"
  '("task with @the-context")
  (next-action TODO.TXT #:context "the-context"))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("this is not a@context"
				 "thissinot@context.org"))
(test-assert "A-context-is-preceded-by-a-single-space-and-an-@"
  (null? (next-action TODO.TXT #:context "context")))
(clear-test-files)

(append-tasks-to-file TODO.TXT '("task with @dummy-context"
				 "task with @a-context and @another-context"
				 "(A) task without context"))
(test-equal "A-task-may-have-zero-one-or-more-than-one-contexts-included-in-it"
  '("task with @a-context and @another-context")
  (next-action TODO.TXT #:context "a-context"))
(test-equal "A-task-may-have-zero-one-or-more-than-one-contexts-included-in-it-Bis"
  '("task with @a-context and @another-context")
  (next-action TODO.TXT #:context "another-context"))
(clear-test-files)

(test-end "cybo-harness")
