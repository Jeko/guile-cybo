(use-modules (system vm coverage)
	     (system vm vm)
	     (srfi srfi-11))

(let ((output-directory (string-append
			 (getenv "HOME")
			 "/Workspace/guile-cybo/coverage")))
  (let-values (((data . values)
		(with-code-coverage
		 (lambda ()
		   (load "cybo-test.scm")))))
    (let* ((port (mkstemp! (string-copy "/tmp/cybo-coverage-XXXXXX")))
	   (file (port-filename port)))
      (coverage-data->lcov data port)
      (close port)
      (when (not (zero? (system* "genhtml" file "-o" output-directory)))
	(error "genhtml failed"))
      (delete-file file))))

